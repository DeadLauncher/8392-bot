from peewee import MySQLDatabase, Model, SQL
from peewee import AutoField, PrimaryKeyField, ForeignKeyField, IntegerField
from peewee import CharField, TextField, BooleanField, DateTimeField, CompositeKey
from config import db_name, user, password

dbhandle = MySQLDatabase(
    db_name, user=user,
    password=password,
    host='localhost'
)

class DBModel(Model):
    class Meta:
        database = dbhandle

class Teacher(DBModel):
    id = AutoField()
    first_name = CharField(max_length = 16)
    last_name = CharField(max_length = 16)
    patronymic = CharField(max_length = 16)

    def get_initials(self):
        name = self.last_name
        if self.first_name:
            name = f'{name} {self.first_name[0]}.'
            if self.patronymic:
                name = f'{name} {self.patronymic[0]}.'
        return name

    class Meta:
        db_table = 'Teachers'
        order_by = ('id',)

class Schedule(DBModel):
    id = AutoField()
    title = CharField(max_length = 64)
    type = IntegerField()
    teacher = ForeignKeyField(Teacher, to_field = 'id', column_name = 'teacher', null = True)
    day = IntegerField()
    period = IntegerField()
    room = CharField(max_length = 32)
    time = DateTimeField()

    class Meta:
        db_table = 'Schedule'
        order_by = ('day', 'time',)

class Day(DBModel):
    date = AutoField()
    extra = CharField(max_length = 255, null = True)

    class Meta:
        db_table = 'Day'
        order_by = ('date',)

class Class(DBModel):
    id = ForeignKeyField(Schedule, to_field='id', column_name='id')
    date = DateTimeField()
    type = IntegerField()
    task = CharField(max_length = 255, null = True)
    extra = CharField(max_length = 255, null = True)

    class Meta:
        db_table = 'Class'
        order_by = ('id', 'date',)
        primary_key = CompositeKey('id', 'date',)

class Notification(DBModel):
    user_id = IntegerField()
    class_id = ForeignKeyField(Schedule, to_field='id', column_name='class_id')
    date = DateTimeField()
    timer = IntegerField()

    class Meta:
        db_table = 'Notification'
        order_by = ('user_id', 'date', 'class_id', 'timer',)
        primary_key = CompositeKey('user_id', 'date', 'class_id', 'timer',)