from datetime import datetime, timedelta
from db import Notification
from utils import getDayClasses
from strings import closedays

def setNotify(args, attachments, fwd):
    if len(args) == 3:
        if args[1].lower() in closedays:
            if not args[2].isdigit():
                return "Попробуйте:\nуведомлять сегодня/завтра/послезавтра 5\n чтобы уведомлять обо всех парах в указанный день за 5 минут до начала"
            delta = timedelta(days = closedays.index(args[1]))
            date = datetime.now().date() + delta
            classes = getDayClasses(date)
            if classes[0].type == -1:
                classes = []
            has_classes = False
            for cl in classes:
                if datetime.now() < datetime(cl.date.year, cl.date.month, cl.date.day, 0, 0) + cl.id.time:
                    notify = Notification(user_id = args[0], class_id = cl.id.id, date = date, timer = args[2])
                    notify.save(force_insert = True)
                    has_classes = True
            if not has_classes:
                return "В указанный день пар нет"
            else:
                return "Уведомления включены"