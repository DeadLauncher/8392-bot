from datetime import datetime, timedelta
from db import Notification

def checkNotify():
    cur = Notification.select().where(Notification.date == datetime.now().date()).order_by(Notification.timer.desc())
    notifies = []
    for notify in cur:
        if (datetime(notify.date.year, notify.date.month, notify.date.day, 0, 0) + notify.class_id.time) > (datetime.now() + timedelta(minutes = notify.timer)):
            continue
        text = "%s через %i мин."%(notify.class_id.title, notify.timer)
        peer = notify.user_id
        notify.delete_instance()
        notifies.append((peer, text))
    return notifies