from datetime import datetime, timedelta
from utils import dateFromStr, getDayClasses
from strings import weekdays, types, closedays

def getDaySchedule(args, attachments, fwd):
    if len(args) == 1:
        delta = timedelta(days = 0)
    elif args[1].lower() in closedays:
        delta = timedelta(days = closedays.index(args[1]))
    elif args[1] in weekdays:
        schedule_weekday = weekdays.index(args[1]) % 7
        weekday = datetime.now().date().weekday()
        delta = timedelta(days = (schedule_weekday - weekday) % 7)
    elif "." in args[1]:
        date = dateFromStr(args[1])
        if date and date >= datetime.now().date():
            delta = date - datetime.now().date()
        else:
            return ""
    else:
        return "Использование:\nрасписание\nрасписание сегодня/завтра\nрасписание пн/вт/чт\nрасписание вторник/пятница"
    date = datetime.now().date() + delta
    classes = getDayClasses(date)
    datestr = date.strftime("%d.%m")
    if datetime.now().year < date.year:
        datestr += ".%s"%date.year
    text = "%s (%s)"%(datestr, weekdays[date.weekday()].upper())
    if classes[0].type == -1:
        text += "\nВыходной"
    else:
        for cl in classes:
            text += "\n%s\n%s (%s)\n%s" %((datetime(1,1,1) + cl.id.time).strftime("%H:%M"), cl.id.title.upper(), types[cl.id.type].upper(), cl.id.room)
            if cl.id.teacher:
                text += ", %s" %cl.id.teacher.get_initials()
            text += "\n"
    return text