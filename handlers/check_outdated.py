from datetime import datetime
from db import Class

def checkOutdated():
    outdated = Class.select().where(Class.date < datetime.now().date())
    for cl in outdated:
        cl.delete_instance()