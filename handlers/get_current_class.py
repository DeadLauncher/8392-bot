from datetime import datetime, timedelta
from db import Class
from utils import timeTo, addDayClasses
from strings import nexts, types

def getCurrentClass(args, attachments, fwd):
    now = datetime.now()       
    classes = Class.select().where(Class.date == now.date())
    if len(classes) == 0:
        classes = addDayClasses(now.date())
    if classes[0].type == -1:
        text = "Сегодня нет пар"
    else:
        if len(args) > 2 and args[2] in nexts:
            end = True
            for cl in classes:
                date = datetime(cl.date.year, cl.date.month, cl.date.day, 0, 0) + cl.id.time
                if now < date:
                    text = "Следующая пара %s через %s (%s)"%(cl.id.title, timeTo((date - now).seconds), cl.id.room)
                    end = False
                    break
            if end:
                text = "Больше пар нет"
        else:
            end = True
            for cl in classes:
                date = datetime(cl.date.year, cl.date.month, cl.date.day, 0, 0) + cl.id.time
                if now < date:
                    text = "Следующая пара %s через %s (%s)"%(cl.id.title, timeTo((date - now).seconds), cl.id.room)
                    end = False
                    break
                elif (now - date) < timedelta(minutes = 85):
                    text = "Идёт %s %s (%s)"%(types[cl.id.type], cl.id.title, cl.id.room)
                    end = False
                    break
            if end:
                text = "Пары кончились"
    return text