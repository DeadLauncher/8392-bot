from utils import dateFromStr, getDayClasses

def setHometask(args, attachments, fwd):
    if len(args) < 3:
        return None
    date = dateFromStr(args[2])
    if not date:
        return None
    classes = getDayClasses(date)
    for cl in classes:
        if cl.id.title == args[1]:
            break
        else:
            cl = None
    if not cl:
        return "В указанный день нет такой пары"
    task = ""
    if fwd != []:
        for message in fwd:
            task += message["text"] + "\n"
    cl.id.task = task
    return "ДЗ установлено"