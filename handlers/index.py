from handlers.get_day_schedule import getDaySchedule
from handlers.get_current_class import getCurrentClass
from handlers.thanks import thanks
from handlers.set_hometask import setHometask
from handlers.set_notify import setNotify

handlers = (
    (getDaySchedule, ("расписание", "пары")),
    (getCurrentClass, ("какая аудитория", "где пара", "какая пара", "когда пара")),
    (thanks, ("спасибо", "благодарю")),
    (setHometask, ("дз", "домашка", "задание")),
    (setNotify, ("уведомить", "уведомление", "уведомлять", "уведомления"))
)