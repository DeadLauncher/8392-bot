min_length = 3
max_err_perc = 0.3
letters = {
    'а': r'укевапсми',
    'б': r'бьолдю',
    'в': r'выцукамсч',
    'г': r'гнролш789',
    'д': r'дшщзж.юбл',
    'е': r'екапрн654',
    'ё': r'ё',
    'ж': r'жщзхэ.юд',
    'з': r'зхэждщ0-=',
    'и': r'импрт',
    'й': r'йцыф',
    'к': r'увапе543',
    'л': r'лгшщдбьо',
    'м': r'мсапи',
    'н': r'непрог765',
    'о': r'орнгшльт',
    'п': r'пакенртим',
    'р': r'рпенгоьти',
    'с': r'чвамс',
    'т': r'тироь',
    'у': r'уцывак34',
    'ф': r'фйцычя',
    'х': r'хзжэъ=-',
    'ц': r'цйфыву',
    'ч': r'чяывс',
    'ш': r'шголдщ',
    'щ': r'щшлджз',
    'ъ': r'ьъ\хэ=',
    'ы': r'ыфцйувсчя',
    'ь': r'ътолб',
    'э': r'эхзж.',
    'ю': r'юбдж.',
    'я': r'яфыч',
}

def match(string, sample):
    err = 0
    for i in range(len(string)):
        if i == len(sample):
            break
        if string[i] not in letters[sample[i]]:
            err += 1
    err += abs(len(string) - len(sample))
    if len(string) < min_length and err > 0:
        return False
    perc = err / len(string)
    if perc > max_err_perc:
        return False
    else:
        return True
    