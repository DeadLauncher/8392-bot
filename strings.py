closedays = (
    "сегодня",
    "завтра",
    "послезавтра"
)

weekdays = (
    "пн", "вт", "ср", "чт", "пт", "сб", "вс",
    "понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"
)

types = (
    "лекция",
    "практика",
    "лабораторная"
)

aud = (
    "аудитория",
    "аудитория?"
)

nexts = ("следующая", "следующая?", "некст", "некст?")