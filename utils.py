from db import Schedule, Class
from datetime import datetime

def dateFromStr(string):
    date = string.split(".")
    now = datetime.now()
    year = now.year
    try:
        if now.month > int(date[1]) or (now.month == int(date[1]) and now.day > int(date[0])):
            year += 1
        date = datetime(year, int(date[1]), int(date[0]))
    except:
        return None
    return date.date()

def addDayClasses(date):
    classes = []
    schedule = Schedule.select().where(Schedule.day == date.weekday()).order_by(Schedule.time)
    if len(schedule) == 0:
        cl = Class(date = date, type = -1)
        cl.save(force_insert = True)
        classes.append(cl)
        return classes
    for sch in schedule:
        if sch.period == date.isocalendar()[1] % 2 or sch.period == 2:
            cl = Class(id = sch.id, date = date, type = sch.type)
            cl.save(force_insert = True)
            classes.append(cl)
    return classes

def getDayClasses(date):
    classes = Class.select().join(Schedule).where(Class.date == date).order_by(Schedule.time)
    if len(classes) == 0:
        classes = addDayClasses(date)
    return classes

def timeTo(seconds):
    minutes = seconds // 60
    hours = minutes // 60
    days = hours // 60
    if days > 0:
        return f'{days} д'
    elif hours > 0:
        return f'{hours} ч'
    else:
        return f'{minutes} мин'