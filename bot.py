import logging
from vk_api import Bot
from config import token, group
from handlers.index import handlers
from handlers.check_notify import checkNotify
from handlers.check_outdated import checkOutdated

logging.basicConfig(level=logging.DEBUG)

bot = Bot(token, group)

while True:
    updates = bot.getUpdates()
    if updates == None:
        logging.error('Error getting updates.')
        continue
    for update in updates:
        if update["type"] == "message_new":
            message = update["object"]["message"]
            attachments = message["attachments"]
            fwd = message["fwd_messages"]
            if "message_reply" in message.keys():
                fwd = message["message_reply"]
            peer = message["peer_id"]
            text = [str(message["peer_id"])] + message["text"].lower().strip(" ").split(" ")[1:]
            for handler in handlers:
                for cmd in handler[1]:
                    if message["text"].lower().strip(" ").startswith(cmd):
                        bot.messageSend(peer, handler[0](text, attachments, fwd))
                        break
    notifies = checkNotify()
    for notify in notifies:
        bot.messageSend(notify[0], notify[1])
    checkOutdated()