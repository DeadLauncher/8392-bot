import logging
from requests import get
from requests.exceptions import ReadTimeout
from config import api_version, api_url

def apiRequest(url, **params):
    try:
        resp = get(url, timeout=(10, 30), params=params)
    except ReadTimeout:
        logging.error(f'Error requesting API ({url}). Read timeout.')
        return None
    if resp.status_code != 200:
        logging.error(f'Response status is not OK:\n{resp.content}')
    else:
        logging.debug(f'Got a response OK from {url}:\n{resp.content}')
        return resp.json()

class Bot:
    def __init__(self, token, group_id):
        self.token = token
        self.group_id = group_id
        self.server = None
        self.ts = None
        self.ts_last = None
        self.key = None
        self.getLongPollServer()

    def getLongPollServer(self):
        resp = apiRequest(f'{api_url}groups.getLongPollServer', group_id=self.group_id, access_token=self.token, v=api_version)
        if not resp:
            logging.error('Error getting Long Poll server.')
        else:
            resp = resp['response']
            self.server = resp['server']
            self.key = resp['key']
            self.ts_last = int(resp['ts'])
            self.ts = self.ts_last
            logging.debug('Long Poll server has been successfuly updated.')

    def getUpdates(self):
        resp = apiRequest(self.server, act='a_check', key=self.key, ts=self.ts, wait=20)
        if not resp:
            logging.error('Can not get updates.')
        elif 'failed' in resp:
            logging.debug('Long Poll server is out of date. Getting a new server...')
            self.getLongPollServer()
        else:
            updates = resp['updates']
            if updates == []:
                logging.debug('No updates.')
                return []
            else:
                self.ts += len(updates)
            self.enableGroupOnline()
            return updates
    
    def messageSend(self, peer_id, text):
        resp = apiRequest(f'{api_url}messages.send', peer_id=peer_id, message=text, random_id=0, access_token=self.token, v=api_version)
        if not resp:
            logging.error('Error sending message.')
        return resp
    
    def enableGroupOnline(self):
        resp = apiRequest(f'{api_url}groups.enableOnline', access_token=self.token, v=api_version, group_id=self.group_id)
        if not resp:
            logging.error('Error enabling group online.')
        return resp